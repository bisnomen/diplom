#---
# Excerpted from "Agile Web Development with Rails",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material, 
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose. 
# Visit http://www.pragmaticprogrammer.com/titles/rails4 for more book information.
#---
Depot::Application.routes.draw do


  get "product_category_view/index"
  get 'category' => 'product_category_view#index'

  get "contact/index"

  get "about/index"

  get 'info' => 'info#index'
  get 'about' => 'about#index'
  get 'contact' => 'contact#index'
  get 'admin' => 'admin#index'

  controller :sessions do
    get  'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end
    
    resources :users
    resources :orders
    resources :line_items
    resources :categories
    resources :carts
    resources :products do
    get :who_bought, on: :member
    end
    root to: 'store#index', as: 'store'

end
