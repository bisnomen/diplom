class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories,  {:primary_key => :category_id} do |t|
      t.integer :category_id
      t.string :category_name

      t.timestamps
    end
  end
end
