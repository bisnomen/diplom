class InfoController < ApplicationController
  
  skip_before_filter :authorize

  def index
    @page_info = 'active'
  end

end
