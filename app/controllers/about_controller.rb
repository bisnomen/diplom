class AboutController < ApplicationController

  skip_before_filter :authorize

  def index
    @page_about = 'active'
  end

end
