class ProductCategoryViewController < ApplicationController
  skip_before_filter :authorize

  def index
    @cat = "1"
    @products = Product.where(category_id: @cat)
  end

end
